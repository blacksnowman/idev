<?PHP
abstract class BasicController 
{
	protected $helper, $DB,$params,$fc;
	
	public function __construct() {
		$this->DB = DB::getInstance();
		$this->fc = FrontController::getInstance();
		$this->helper = Helper::getInstance();
		$this->params = $this->fc->getParams();
	}
	
	protected function returnData($retval,$data,$error = null){
		$tmp['result'] = $retval;
		if($retval==true) {
			$tmp['data'] = $data;
		} else {
			$tmp['error'] = $error;
		}
		return $this->helper->retJSON($tmp);
	}
}