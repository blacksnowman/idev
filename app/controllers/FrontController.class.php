<?php
class FrontController implements FrontControllerInterface
{
	protected $_controller, $_action, $_params, $_body;
	protected $splits, $request;
	public $_page;
	public $_page_name;
	public $_chain;
	static $_instance;

	private $defaultController = "ItemsController";
	private $defaultAction = "indexAction";
	private $menu = array("Товары"=>"/items", "Категории"=>"/categories");
	
	public static function getInstance() {
		if(!(self::$_instance instanceof self)) 
			self::$_instance = new self();
		return self::$_instance;
	}
	
	private function splitURL(){
		$this->request = $_SERVER['REQUEST_URI'];
		$this->splits = explode('/', trim($this->request,'/'));
		return $this->splits;
	}
	
	private function readParams(){
		$splits = $this->splits;
		if(!empty($splits[2])){
			$keys = $values = array();
				for($i=2, $cnt = count($splits); $i<$cnt; $i++){
					if($i % 2 == 0){
						//Чётное = ключ (параметр)
						$keys[] = $splits[$i];
					}else{
						//Значение параметра;
						if(preg_match("#([^\?]+)[\?]*#",$splits[$i], $out)){
							$values[] = $out[1];
						}
					}
				}
			$this->_params = array_combine($keys, $values);
		}
	}
	
	private function __construct() {
		$splits = $this->splitURL();
		$this->_page =  empty($splits[0]) ? "main" : trim($splits[0]);
		$this->_controller = !empty($splits[0]) ? ucfirst($splits[0]).'Controller' : $this->defaultController;
		$this->_action = !empty($splits[1]) ? $splits[1].'Action' : $this->defaultAction;
		$this->readParams();
	}
	
	public function route() {
		if(class_exists($this->getController())) {
			$rc = new ReflectionClass($this->getController());
			if($rc->implementsInterface('IController')) {
				if($rc->hasMethod($this->getAction())) {
					$controller = $rc->newInstance();
					$method = $rc->getMethod($this->getAction());
					$method->invoke($controller);
				} else {
					throw new Exception("Action method not implemented");
				}
			} else  {
				throw new Exception("Interface");
			}
		} else {
			try {
				$this->raiseError404();
				throw new Exception("Controller doesn't exists");
			} catch (Exception $e) {
				# echo "<BR>Caught Exception: ", $e->getMessage();
			}
		}
	}
	
	private function raiseError404() {
		header("HTTP/1.0 404 Not Found");
	}
	
	private function logRequest() {
		$request = array(
			"redirectStatus"=>$_SERVER['REDIRECT_STATUS'],
			"requestMethod"=>$_SERVER['REQUEST_METHOD'],
			"remoteAddr"=>$_SERVER['REMOTE_ADDR'],
			"httpUserAgent"=>$_SERVER['HTTP_USER_AGENT'],
			"requestURI"=>$_SERVER['REQUEST_URI'],
			"requestTime"=>$_SERVER['REQUEST_TIME']
			);
		# $logger = new requestLogger($request);
		# $logger->logRequest();
	}
	
	public function getParams() {
		return $this->_params;
	}
	
	public function getController() {
		return $this->_controller;
	}
	
	public function getPage() {
		return $this->_page;
	}
	
	public function getMenu() {
		return $this->menu;
	}
	
	public function getAction() {
		return $this->_action;
	}
	
	public function getBody() {
		return $this->_body;
	}
	
	public function getRequest(){
		return $this->request;
	}
	
	public function setBody($body) {
		$this->_body = $body;
	}
}	
?>