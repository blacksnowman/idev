<?PHP
class ItemsController extends BasicController implements IController 
{
	public function __construct() {
		parent::__construct();
	}
	
	public function indexAction(){
		$this->menu = $this->fc->getMenu();
		$view = new View("items.layout.php", array(
				"title" => "Товары",
				"menu"=>$this->menu,
				"js"=> "items.js",
			)
		);
		$view->render();
	}
	
	public function listAction(){
		$item = new Item();
		$items = $item->getAll();
		if(count($items)>0) {
			$this->returnData(true,$items);
		} else {
			$this->returnData(false,"not found");
		}
	}
	
	public function editAction(){
		if(isset($this->params['id'])&&($this->params['id']>0)) {
			$id = $this->params['id'];
			
			/* load item by id */
			$item = new Item();
			$retval = $item->getById($id);
			
			$this->returnData($retval,$item,$item->getError());
		} else {
			$this->returnData(false,$item,"Wrong id");
		}
	}
	
	public function insertAction(){
		if($this->validate($_POST)) {
			$item = new Item();
			$item->name = $this->request['name'];
			$item->price = $this->request['price'];
			$item->cat_id = $this->request['cat_id'];
			$retval = $item->create();
			$this->returnData($retval,$item,$item->getError());
		} else {
			$this->returnData(false,$item,"Wrong data");
		}
	}
	
	private function validate($post){
		foreach($post as $k=>$v){
			$this->request[$k] = trim($v);
		}
		
		if(isset($post['name'])&&!empty($post['name'])&&(strlen($post['name'])<=255)) {
			$this->request['name'] = trim($post['name']);
		} else {
			return false;
		}
		
		if(isset($post['price'])&&($post['price']>0)) {
			$this->request['price'] = trim($post['price']);
		} else {
			return false;
		}
		
		if(isset($post['cat_id'])&&($post['cat_id']>0)) {
			$this->request['cat_id'] = trim($post['cat_id']);
		} else {
			return false;
		}
		return true;
	}
	
	public function updateAction() {
		if($this->validate($_POST)&&($_POST['id']>0)) {
			$item = new Item();
			if($item->getById($this->request['id'])) {
				$item->name = $this->request['name'];
				$item->price = $this->request['price'];
				$item->cat_id = $this->request['cat_id'];
				$retval = $item->update();
			} else {
				$retval = false;
			}
			$this->returnData($retval,$item,$item->getError());
		} else {
			$this->returnData(false,$item,"Wrong data");
		}
	}
	
	public function deleteAction(){
		if(isset($_POST['id'])&&($_POST['id']>0)) {
			$id = (int)$_POST['id'];
			$item = new Item();
			$retval = $item->delete($id);
			$this->returnData($retval,$id,$item->getError());
		}else {
			$this->returnData(false,null,"Wrong ID");
		}
	}
}
?>