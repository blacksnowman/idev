<?PHP
class ApiController implements IController 
{
	private $DB;
	private $params;
	private $helper;
	private $request;
	private $validationFlag=false;
	private $validationError = array();
	private $maxLen = 512;
	
	public function indexAction(){
		$info = ['verion'=>'1.0', 'name'=>'Web API'];
		$tmp = array_merge($info,$_SERVER);
		return $this->helper->retJSON($tmp);
	}
	
	public function phpinfoAction(){
		phpinfo();
	}
	
	public function getItemsAction(){

	}
	
	public function __construct() {
		$this->DB = DB::getInstance();
		$this->fc = FrontController::getInstance();
		$this->helper = Helper::getInstance();
		$this->params = $this->fc->getParams();
	}
}