<?php
interface IController 
{
	public function indexAction();
	public function listAction();
	public function editAction();
	public function insertAction();
	public function updateAction();
	public function deleteAction();
}

?>