<?PHP
class CategoriesController extends BasicController implements IController 
{
	public function __construct() {
		parent::__construct();
	}
	
	public function indexAction(){
		$this->menu = $this->fc->getMenu();
		$view = new View("categories.layout.php", array(
				"title" => "Категории",
				"menu"=>$this->menu,
				"js"=>"categories.js"
			)
		);
		$view->render();
	}
	
	public function listAction(){
		$cat = new Category();
		$cats = $cat->getAll();
		if(count($cats)>0) {
			$this->returnData(true,$cats);
		} else {
			$this->returnData(false,"not found");
		}
	}
	
	public function listViewAction(){
		$cat = new Category();
		$cats = $cat->getAllView();
		if(count($cats)>0) {
			$this->returnData(true,$cats);
		} else {
			$this->returnData(false,"not found");
		}
	}
	
	public function editAction() {
		if(isset($this->params['id'])&&($this->params['id']>=0)) {
			$id = $this->params['id'];
			# show_arr($this->params);
			$cat = new Category();
			$retval = $cat->getById($id);
			$this->returnData($retval,$cat,$cat->getError());
		} else {
			$this->returnData(false,$cat,"Wrong id");
		}
	}
	
	public function insertAction(){
		if($this->validate($_POST)) {
			
			$cat = new Category();
			$cat->name = $this->request['name'];
			$cat->parent_id = $this->request['parent_id'];
			$retval = $cat->create();
			$this->returnData($retval,$cat,$cat->getError());
		} else {
			
			$this->returnData(false,$cat,"Wrong data");
		}
	}
	
	public function updateAction(){
		if($this->validate($_POST)&&($_POST['id']>=0)) {
			$cat = new Category();
			if($cat->getById($this->request['id'])) {
				$cat->name = $this->request['name'];
				$cat->parent_id = $this->request['parent_id'];
			
				$retval = $cat->update();
			} else {
				$retval = false;
			}
			$this->returnData($retval,$cat,$cat->getError());
		} else {
			$this->returnData(false,$cat,"Wrong data");
		}
	}
	
	public function deleteAction(){
		if(isset($_POST['id'])&&($_POST['id']>0)) {
			$id = (int)$_POST['id'];
			$cat = new Category();
			$retval = $cat->delete($id);
			$this->returnData($retval,$id,$cat->getError());
		}else {
			$this->returnData(false,null,"Wrong ID");
		}
	}
	
	private function validate($post){
		foreach($post as $k=>$v){
			$this->request[$k] = trim($v);
		}
		return true;
	}
}
?>