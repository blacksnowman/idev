<?PHP
interface FrontControllerInterface
{
    public function getController();
    public function getAction();
    public function getParams();
    public function route();
}
?>