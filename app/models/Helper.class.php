<?PHP
class Helper
{
	static $_instance;
	
	public static function getInstance() {
		if(!(self::$_instance instanceof self)) 
			self::$_instance = new self();
		return self::$_instance;
	}
	
	public function xscanDir($dir,$ext = null) {
		$d = dir($dir);
		while (false !== ($entry = $d->read())) {
		   if (($entry!=".") AND ($entry!="..")) {
				if($ext!=null){
					if(strpos($entry, $ext) OR strpos($entry, strtoupper($ext))) {
						$tmp[] = $entry;
					}
				} else {
					$tmp[] = $entry;
				}
			}
		}
		$d->close();
		return $tmp;
	}
	
	public function xreadDir($dir) {
		$d = dir($dir);
		while (false !== ($entry = $d->read())) {
		   if (($entry!=".") AND ($entry!="..")) {
				if(is_dir(sprintf("%s/%s",$dir,$entry))){
					$tmp[] = $entry;
				}
			}
		}
		$d->close();
		return $tmp;
	}
	
	public function retJSON($json) {
		header('Content-Type: application/json');
		echo json_encode($json);
	}
}
?>