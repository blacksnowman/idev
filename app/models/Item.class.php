<?PHP
class Item implements iModel
{
	public $id, $name, $price, $cat_id;
	private $DB,$error;
	
	public static function getInstance() {
		if(!self::$_instance instanceOf self)
			self::$_instance = new self;
		return self::$_instance;
	}
	
	public function __construct() {
		$this->DB = DB::getInstance();
	}
	
	public function getError(){
		return $this->error;
	}
	
	public function getAll($offset = null, $limit = null){
		if(isset($limit)&&($limit>0)) {
			if(isset($offset)&&($offset>0)&&($offset<=$limit)) {
				$limitSql = sprintf("LIMIT %d,%d", $offset, $limit);
			}
		}
		$sql = "SELECT i.id, i.`name` as item, i.price,  c.`name` as category, i.updated FROM items i LEFT JOIN categories c ON (c.id=i.cat_id) $limitSql";
		$res = $this->DB->getResults($sql);
		return $res;
	}
	
	public function getById($id){
		$sql = "SELECT * FROM items WHERE id=?";
		$sth = $this->DB->prepare($sql);
		$sth->bindParam(1,$id);
		$sth->execute();
		$count = $sth->rowCount();
		
		if($count>0){
			$result = $sth->fetch(PDO::FETCH_ASSOC);
			foreach($result as $key=>$val) {
				$this->$key = $val;
			}
			return true;
		} else {
			$this->error = "Not found";
			return false;
		}
	}
	
	public function update(){
		try {
			$sql = "UPDATE items SET `name`=:name, `price`=:price, `updated`=NOW(), cat_id=:cat_id WHERE id=:id";
			$sth = $this->DB->prepare($sql);
			$sth->bindParam(":name",$this->name);
			$sth->bindParam(":price",$this->price);
			$sth->bindParam(":cat_id",$this->cat_id);
			$sth->bindParam(":id",$this->id);
			$sth->execute();
			return true;
		} catch(PDOException $e) {
			# echo $e->getMessage();
			$this->error = $e->getMessage();
			return false;
		}
	}
	
	public function create(){
		try {
			$sql = "INSERT INTO items(`name`,`price`,`cat_id`,`updated`) VALUES(?,?,?,NOW())";
			$sth = $this->DB->prepare($sql);
			$sth->bindParam(1,$this->name);
			$sth->bindParam(2,$this->price);
			$sth->bindParam(3,$this->cat_id);
			$sth->execute();
			
			$this->id = $this->DB->lastInsertId(); 
			return true;
		} catch(PDOException $e) {
			# echo $e->getMessage();
			$this->error = $e->getMessage();
			return false;
		}
	}
	
	public function delete($id){
		try {
			$sql = "DELETE FROM items WHERE id=?";
			$sth = $this->DB->prepare($sql);
			$sth->bindParam(1,$id);
			$sth->execute();
			if($sth->rowCount()) {
				return true;
			} else {
				$this->error = "No records deleted";
				return false;
			}
		} catch(PDOException $e) {
			# echo $e->getMessage();
			$this->error = $e->getMessage();
			return false;
		}
	}
}
?>