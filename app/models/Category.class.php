<?PHP
class Category implements iModel
{
	private $DB,$error;
	private $tableName = "categories";
	private $chain;
	static $_instance;
	
	public $id, $name,$added, $parent_id;
	
	public static function getInstance() {
		if(!self::$_instance instanceOf self)
			self::$_instance = new self;
		return self::$_instance;
	}
	
	public function __construct() {
		$this->DB = DB::getInstance();
	}
	
	public function getError(){
		return $this->error;
	}
	
	public function getAll($offset = null, $limit = null) {
		if(isset($limit)&&($limit>0)) {
			if(isset($offset)&&($offset>0)&&($offset<=$limit)) {
				$limitSql = sprintf("LIMIT %d,%d", $offset, $limit);
			}
		}
		$sql = sprintf("SELECT * FROM %s $limitSql", $this->tableName);
		# $sql = "SELECT * FROM categories";
		$res = $this->DB->getResults($sql);
		return $res;
	}
	
	public function getAllView($offset = null, $limit = null) {
		if(isset($limit)&&($limit>0)) {
			if(isset($offset)&&($offset>0)&&($offset<=$limit)) {
				$limitSql = sprintf("LIMIT %d,%d", $offset, $limit);
			}
		}
		
		$sql = "SELECT c1.id,c1.`name` as category,  c2.`name` as parent, c1.added
					FROM categories c1
					LEFT JOIN categories c2 ON (c1.parent_id=c2.id)
					WHERE c1.parent_id=c2.id AND c1.id<>0;";
		$res = $this->DB->getResults($sql);
		return $res;
	}
	
	public function getById($id) {

		$sql = sprintf("SELECT * FROM %s WHERE id=?", $this->tableName);
		$sth = $this->DB->prepare($sql);
		$sth->bindParam(1,$id);
		$sth->execute();
		$count = $sth->rowCount();
		
		if($count>0){
			$result = $sth->fetch(PDO::FETCH_ASSOC);
			foreach($result as $key=>$val) {
				$this->$key = $val;
			}
			return true;
		} else {
			$this->error = "Not found";
			return false;
		}
	}
	
	public function buildChain($pid){
		$sql = sprintf("SELECT * FROM %s WHERE perent_id=:pid", $this->tableName);
		$sth = $this->DB->prepare($sql);
		$sth->bindParam(":pid",$pid);
		$sth->execute();
		$res = $this->DB->getResults($sql);
	}
	
	public function update() {
		try {
			$sql = sprintf("UPDATE %s SET `name`=:name, `added`=NOW() ,`parent_id`=:parent_id WHERE id=:id", $this->tableName);
			$sth = $this->DB->prepare($sql);
			$sth->bindParam(":name",$this->name);
			$sth->bindParam(":parent_id",$this->parent_id);
			$sth->bindParam(":id",$this->id);
			$sth->execute();
			return true;
		} catch(PDOException $e) {
			# echo $e->getMessage();
			$this->error = $e->getMessage();
			return false;
		}
	}
	
	public function create(){
		try {
			$this->added = date("Y-m-d H:i:s");
			$sql = sprintf("INSERT INTO %s(`name`,`added`,`parent_id`) VALUES(?,?,?)", $this->tableName);
			$sth = $this->DB->prepare($sql);
			$sth->bindParam(1,$this->name);
			$sth->bindParam(2,$this->added);
			$sth->bindParam(3,$this->parent_id);
			$sth->execute();
			$this->id = $this->DB->lastInsertId(); 
			return true;
		} catch(PDOException $e) {
			# echo $e->getMessage();
			$this->error = $e->getMessage();
			return false;
		}
	}
	
	public function delete($id){
		try {
			$sql = sprintf("DELETE FROM %s WHERE id=?", $this->tableName);
			$sth = $this->DB->prepare($sql);
			$sth->bindParam(1,$id);
			$sth->execute();
			
			if( $sth->rowCount()) {
				return true;
			} else {
				$this->error = "No records deleted";
				return false;
			}
		} catch(PDOException $e) {
			# echo $e->getMessage();
			$this->error = $e->getMessage();
			return false;
		}
	}
}
?>