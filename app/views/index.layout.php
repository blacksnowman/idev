<!DOCTYPE html> 
<html>
<head>
	<title><?=$this->title?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<?PHP foreach ($this->css as $k=>$link){ 
		echo "<link rel='stylesheet' href='/css/$link'>";
	} ?>
</head>
<body id="body">
<div class="container">
	<div class="row">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="#">Task 2</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">
				<?PHP
				foreach($this->menu as $menuName => $menuLink) {
					echo "<li><a href='$menuLink'>$menuName</a></li>";
				}
				?>
			  </ul>
			</div>
		</div>
		</nav>
	</div>
	<div class="row">
		<div id="contentBlock" class="col-md-10 col-offset-1">
		<div id="alertBlock" style="display: none;"></div>
		<?PHP include($this->layout); ?>
		</div>
	</div>
</div>
<?PHP require_once("modal.layout.php"); ?>
</body>
<?PHP foreach ($this->js as $k=>$script){ 
	echo "<script type='text/javascript' src='/js/$script'></script>";
} ?>
</html>