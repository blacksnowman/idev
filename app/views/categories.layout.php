<?PHP  $this->extendLayout("index.layout.php"); ?>
<div class="table-responsive">
	<table class="table table-striped table-hover"  cellpadding=0 cellspacing=0>
	<thead>
		<tr>
			<th width=110>&nbsp;</th>
			<th class="hidden-xs">ID</th>
			<th>Название</th>
			<th class="hidden-xs">Родитель</th>
			<th class="hidden-xs">Добавлена</th>
		</tr>
	</thead>
	<tbody id="catsTable"></tbody>
	</table>
</div>
<div class="btn_block">
	<button class="btn" type="button" onClick='addCat();'>Добавить</button>
</div>
