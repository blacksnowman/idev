<?PHP  $this->extendLayout("index.layout.php"); ?>
<div class="table-responsive">
	<table class="table table-striped table-hover"  cellpadding=0 cellspacing=0>
	<thead>
		<tr>
			<th width=110>&nbsp;</th>
			<th class="hidden-xs">ID</th>
			<th>Название</th>
			<th>Цена</th>
			<th class="hidden-xs">Категория</th>
			<th class="hidden-xs">Дата обновления</th>
		</tr>
	</thead>
	<tbody id="itemsTable"></tbody>
	</table>
</div>
<div class="btn_block">
	<button class="btn" type="button" onClick='raiseAlert(0,"Вызов уведомления");'>Проверка</button>
	<button class="btn" type="button" onClick='addItem();'>Добавить</button>
</div>
