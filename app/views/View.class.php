<?PHP
class View {
	private $layout;
	public $counter = 0;
	
	public $js = array(
		"jquery-3.2.1.min.js",
		"bootstrap.min.js"
		);
	
	public $css = array(
		"bootstrap.min.css",
		"bootstrap-theme.min.css",
		"common.css",
	);
	
	public function __construct($layout,$data) {
		$this->layout = $layout;
		$this->title = $data['title'];
		$this->menu = $data['menu'];
		$this->setJs($data['js']);
		$this->setCSS($data['css']);
	}
	
	private function setJs($js) {
		if(isset($js)){
			if(is_array($js)) {
				$this->js = array_merge($this->js,$js);
			}else {
				array_push($this->js, $js);
			}
		}
	}
	
	private function setCSS($css) {
		if(isset($css)){
			if(is_array($css)) {
				$this->js = array_merge($this->js,$css);
			}else {
				array_push($this->js, $css);
			}
		}
	}
	
	public function render() {
		$this->flag = false;
		include($this->layout);
	}
	
	public function extendLayout($extend) {
		$this->extend=$extend;
		if($this->flag===true) {
			# nothing to do, read till the end of layout
		} else {
			# first pass, build parent layout
			$this->flag = true;
			include_once($this->extend);
			exit();
		}
	}
}	
?>