<div id="myModal" class="modal fade">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			 <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 id="modalTitle" class="modal-title">Modal Header</h4>
			</div>
			<div id="modalBlock" class="modal-body"></div>
			<div id="modalButtons" class="modal-footer"></div>
		</div>
	</div>
</div>