<?PHP
function __autoload($class_name) {
	include_once($class_name.".class.php");
}

function show_arr($arr) {
	foreach ($arr as $key => $val) {
		echo "\n<ul>";
		if (is_array($val)) {
			echo "<il>&nbsp;&nbsp;[<font color=#888888>$key</font>] = >[<font color=#AA5555>$val</font>]</il>\n";
			echo show_arr($val);
		} else {
			echo "<il>&nbsp;&nbsp;[<font color=#888888>$key</font>] => [<font color=#AA5555>$val</font>]</il>\n";
		}
		echo "\n</ul>";
	}
}
?>