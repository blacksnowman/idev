$(document).ready(function() {
	loadItems();
	$("#myModal").on('hide.bs.modal', function () {
    		$("#modalBlock").empty();
			$("#modalTitle").empty();
			$("#modalButtons").empty();
    });
});


/* load full list of items */
function loadItems(){
	var url = "/items/list";
	//getJSON(url,buildTable);
	$.getJSON(url,function(json) {
		buildTable(json);
	});
}

/* press button to edit item */
function editItem(id){
	var url =  "/items/edit/id/"+id;
	$.getJSON(url,function(json) {
		if(json.result==true){
			$('#myModal').modal('show');
			buildEditFrom(json.data, 1);
		} else {
			raiseAlert(3,"failed to get data from "+url);
		}
	});
}

/* press button to create new item */
function addItem(){
	$('#myModal').modal('show');
	buildEditFrom();
}

/* press button to delete item */
function deleteItem(id,name){
	$('#modalTitle').text("Внимание!");
	$('#modalBlock').html("Вы действительно хотите удалить товар  (ID="+id+"), <strong>"+name+"</strong>?");
	/* clear modal buttons */
	$("#modalButtons").empty();
	/* add new buttond */
	var btn = $('<button>Удалить</button>');
	btn.attr("type","button");
	btn.attr("onClick","deleteRecord("+id+")");
	btn.addClass("btn btn-primary");
	btn.attr("data-dismiss","modal");
	btn.appendTo($("#modalButtons"));
	
	var btn = $('<button>Отмена</button>');
	btn.attr("type","button");
	btn.attr("data-dismiss","modal");
	btn.addClass("btn btn-secondary");
	btn.appendTo($("#modalButtons"));
	$('#myModal').modal('show');
}

/* delete item: request to API */
function deleteRecord(id){
	$.post( "/items/delete", { id: id }, function(json) {
		if(json.result==true) {
			raiseAlert(0,"Товар с идентификатором ID="+id+" успешно удален");
			loadItems();
		} else {
			raiseAlert(0,"Товар с идентификатором ID="+id+" не удалось удалить! Код ошибки: "+json.error);
		}
	});
}

/* update item: request to API */
function saveRecord(id){
	var name = $("#itemName").val();
	var price =  $("#itemPrice").val();
	var cat =  $("#categoryId").val();
	
	$.post( "/items/update", { id: id, name: name, cat_id: cat, price: price }, function(json) {
		if(json.result==true) {
			loadItems();
			raiseAlert(0,"Товар с идентификатором ID="+id+" успешно отредактирован");
		} else {
			raiseAlert(3,"Товар с идентификатором ID="+id+" не удалось отредактировать! Код ошибки: "+json.error);
		}
	});
}

/* insert item: POST request with data to API */
function insertRecord() {
	var name = $("#itemName").val();
	var price =  $("#itemPrice").val();
	var cat =  $("#categoryId").val();
	
	$.post( "/items/insert", { name: name, cat_id: cat, price: price }, function(json) {
		if(json.result==true) {
			loadItems();
			raiseAlert(0,"Товар с идентификатором "+name+" успешно добавлен");
		} else {
			raiseAlert(3,"Товар не удалось добавить! Код ошибки: "+json.error);
		}
	});
}


/* raise alert for user */
function raiseAlert(level, msg){
	removeAlertBlock();
	setTimeout(	removeAlertBlock, 5000);
	switch (level) {
		case 0:
			warningClass = "alert-success";
		break;
		
		case 1:
			warningClass = "alert-info";
		break;
		
		case 2:
			warningClass = "alert-warning";
		break;
		
		case 3:
			warningClass= "alert-danger";
		break;
	};
	
	var div = document.createElement("div");
	div.className = "alert "+warningClass+" alert-dismissible"; //class="hide fade"
	div.id = "alertMessage";
	
	var btn = document.createElement("button");
	btn.type="button";
	btn.className="close";
	btn.setAttribute("data-dismiss","alert");
	btn.innerHTML = "&times;";
	div.appendChild(btn);
	
	var span = document.createElement("strong");
	span.innerHTML = "Внимание!";
	div.appendChild(span);
	
	var text = document.createElement("span");
	text.innerHTML = " "+msg;
	div.appendChild(text);

	var contentBlock = document.getElementById("alertBlock"); 
	contentBlock.appendChild(div);
	
	$('#alertBlock').fadeIn(300);
}

/* remove alert block */
function removeAlertBlock(){
	var alertMessage = document.getElementById("alertMessage"); 
	var contentBlock = document.getElementById("alertBlock"); 
	if(alertMessage) {
		$('#alertBlock').fadeOut(600, function() {   
			contentBlock.removeChild(alertMessage);
		});
		return true;
	} else {
		return false;
	}
}

/* build item form: edit and create 
action: 0 to create new item;
action: 1 to update existing item;
*/
function buildEditFrom(item, action){
	$('#modalTitle').text("Редактирование товара");
	
	var form = $("<form></form>");
	form.attr("id", "itemForm");
	
	var div = $("<div></div>");
	div.addClass("form-group");
	div.append('<label for="itemName">Наименование</label>');
	
	var input = $("<input></input>");
	input.attr("type","text");
	input.addClass("form-control");
	input.attr("id","itemName");
	if(action==1) {
		input.attr("value", item.name);
	}
	input.appendTo(div);
	div.appendTo(form);
	
	var div = $("<div></div>");
	div.addClass("form-group");
	div.append("<label for='itemPrice'>Цена</label>");
	
	var input = $("<input></input>");
	input.attr("type","number");
	input.addClass("form-control");
	input.attr("id","itemPrice");
	if(action==1) {
		input.attr("value", item.price);
	}
	input.appendTo(div);
	div.appendTo(form);
	
	var div = $("<div></div>");
	div.addClass("form-group");
	div.attr("id","selectCat");
	div.append("<label for='itemPrice'>Категория товара</label>");
	
	var cat_id =0;
	if(action==1) { 
		cat_id = item.cat_id;
	} 
	
	$.get( "/categories/list", function(json) {
		if(json.result==true){
			var categories = json.data;
			var select = $("<select class='form-control'></select>");
			select.attr("id","categoryId");
			for(var i=0;i<categories.length;i++){
				if(categories[i].id==cat_id) {
					select.append("<option selected value="+categories[i].id+">"+categories[i].name+"</option>");
				}else {
					select.append("<option value="+categories[i].id+">"+categories[i].name+"</option>");
				}
			}
			div.append(select);
		}
	});
		
	div.appendTo(form);
	
	$('#modalBlock').append(form);
	
	var btn = $('<button>Сохранить</button>');
	btn.attr("type","button");
	if(action==1) {
		btn.attr("onClick","saveRecord("+item.id+")");
	} else {
		btn.attr("onClick","insertRecord()");
	}
	btn.addClass("btn btn-primary");
	btn.attr("data-dismiss","modal");
	btn.appendTo($("#modalButtons"));
	
	btn = $('<button>Отмена</button>');
	btn.attr("type","button");
	btn.attr("data-dismiss","modal");
	btn.addClass("btn btn-secondary");
	btn.appendTo($("#modalButtons"));
}

/* vanilla JS HTTP GET request to API */
function getJSON(url,callback){
	var xhr = new XMLHttpRequest();
	xhr.open("GET", url, true)
	xhr.onreadystatechange = function () {
		if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
			callback(JSON.parse(xhr.responseText));
		} 
		
		if (xhr.readyState == XMLHttpRequest.DONE && xhr.status != 200) {
			alert("HTTP: "+url+", "+xhr.status);
		};
	};
	xhr.send(body);
}

/* draw table of items */
function buildTable(json){
	if(json.result==true) {
		var data = json.data;
		var table = document.getElementById("itemsTable");
		while (table.firstChild) {
			table.removeChild(table.firstChild);
		}
		
		for(var i=0;i<data.length; i++) {
			var id = data[i].id;
			var tr = document.createElement("tr");
			var td = document.createElement("td");
			
			var btn = editBtn(id);
			td.appendChild(btn);
			
			var btn = deleteBtn(id,data[i].item);
			td.appendChild(btn);
			
			tr.appendChild(td);
			
			for (var key in data[i]) {
				var value = data[i][key];
				var td = document.createElement("td");
				if((key=="id")||(key=="category")||(key=="updated")) {
					td.className = "hidden-xs";
				}
				td.id = key;
				
				var text = document.createTextNode(value);
				td.appendChild(text);
				tr.appendChild(td);
			}
			table.appendChild(tr);
		}
	} else {
		console.log("Failed to fetch data");
	}
}

function editBtn(id) {
	var btn = document.createElement("button");
	btn.type="button";
	btn.className="btn btn-default";
	btn.setAttribute("aria-label","Left Align");
	
	var span = document.createElement("span");
	span.className = "glyphicon glyphicon-pencil";
	span.setAttribute("aria-hidden","true");
	btn.appendChild(span);
	btn.setAttribute("onClick", "editItem("+id+")");
	return btn;
}

function deleteBtn(id,name){
	var btn = document.createElement("button");
	btn.type="button";
	btn.className="btn btn-default";
	btn.setAttribute("aria-label","Left Align");
	
	var span = document.createElement("span");
	span.className = "glyphicon glyphicon-trash";
	span.setAttribute("aria-hidden","true");
	btn.appendChild(span);

	btn.setAttribute("onClick", "deleteItem("+id+", '"+name+"')");
	return btn;
}
