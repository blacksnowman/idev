$(document).ready(function() {	
	loadCats();
	$("#myModal").on('hide.bs.modal', function () {
    		$("#modalBlock").empty();
			$("#modalTitle").empty();
			$("#modalButtons").empty();
    });
});

function loadCats(){
	$.getJSON("/categories/listview",function(json) {
		if(json.result==true){
			buildTable(json.data);
		} else {
			console.log(json.error);
		}
	});
}

/* press button to edit item */
function editCat(id){
	var url =  "/categories/edit/id/"+id;
	$.getJSON(url,function(json) {
		if(json.result==true){
			$('#myModal').modal('show');
			buildEditFrom(json.data, 1);
		} else {
			raiseAlert(3,"failed to get data from "+url);
		}
	});
}

/* press button: add new category */
function addCat(){
	$('#myModal').modal('show');
	buildEditFrom(null, 0);
}

/* press button to delete item */
function deleteCat(id,name){
	$('#modalTitle').text("Внимание!");
	$('#modalBlock').html("Вы действительно хотите удалить категорию  (ID="+id+"), <strong>"+name+"</strong>?");
	/* clear modal buttons */
	$("#modalButtons").empty();
	/* add new buttond */
	var btn = $('<button>Удалить</button>');
	btn.attr("type","button");
	btn.attr("onClick","deleteRecord("+id+")");
	btn.addClass("btn btn-primary");
	btn.attr("data-dismiss","modal");
	btn.appendTo($("#modalButtons"));
	
	var btn = $('<button>Отмена</button>');
	btn.attr("type","button");
	btn.attr("data-dismiss","modal");
	btn.addClass("btn btn-secondary");
	btn.appendTo($("#modalButtons"));
	$('#myModal').modal('show');
}


/* delete category: request to API */
function deleteRecord(id){
	$.post( "/categories/delete", { id: id }, function(json) {
		if(json.result==true) {
			raiseAlert(0,"Товар с идентификатором ID="+id+" успешно удален");
			loadCats();
		} else {
			raiseAlert(0,"Товар с идентификатором ID="+id+" не удалось удалить! Код ошибки: "+json.error);
		}
	});
}

/* update category: request to API */
function saveRecord(id){
	var name = $("#catName").val();
	var parent =  $("#catParent").val();
	
	$.post( "/categories/update", { id: id, name: name, parent_id: parent }, function(json) {
		if(json.result==true) {
			loadCats();
			raiseAlert(0,"Товар с идентификатором ID="+id+" успешно отредактирован");
		} else {
			raiseAlert(3,"Товар с идентификатором ID="+id+" не удалось отредактировать! Код ошибки: "+json.error);
		}
	});
}

/* insert category: POST request with data to API */
function insertRecord() {
	var name = $("#catName").val();
	var parent =  $("#catParent").val();
	
	$.post( "/categories/insert", {  name: name, parent_id: parent  }, function(json) {
		if(json.result==true) {
			loadCats();
			raiseAlert(0,"Товар с идентификатором "+name+" успешно добавлен");
		} else {
			raiseAlert(3,"Товар не удалось добавить! Код ошибки: "+json.error);
		}
	});
}

function buildTable(data){
	var table = $("#catsTable");
	$("#catsTable").empty();
	for(var i=0;i<data.length; i++) {
		var id = data[i].id;
		var tr = $("<tr></tr>");
		var td = $("<td></td>");
		
		var btn =  editBtn(id);
		td.append(btn);
		
		var btn =  deleteBtn(id,data[i].name);
		td.append(btn);

		td.appendTo(tr);

		for (var key in data[i]) {
			if((key=="id")||(key=="parent")||(key=="added")) {
				tr.append("<td class='hidden-xs'>"+data[i][key]+"</td>")
			} else {
				tr.append("<td>"+data[i][key]+"</td>")
			}
		}
		
		$("#catsTable").append(tr);
	}
}

function editBtn(id) {
	var btn = document.createElement("button");
	btn.type="button";
	btn.className="btn btn-default";
	btn.setAttribute("aria-label","Left Align");
	
	var span = document.createElement("span");
	span.className = "glyphicon glyphicon-pencil";
	span.setAttribute("aria-hidden","true");
	btn.appendChild(span);
	btn.setAttribute("onClick", "editCat("+id+")");
	return btn;
}

function buildEditFrom(cat, action){
	$('#modalTitle').text("Редактирование товара");
	
	var form = $("<form></form>");
	form.attr("id", "itemForm");
	
	var div = $("<div></div>");
	div.addClass("form-group");
	div.append('<label for="catName">Наименование</label>');
	
	var input = $("<input></input>");
	input.attr("type","text");
	input.addClass("form-control");
	input.attr("id","catName");
	if(action==1) {
		input.attr("value", cat.name);
	}
	input.appendTo(div);
	div.appendTo(form);
	
	var div = $("<div></div>");
	div.addClass("form-group");
	div.attr("id","selectCat");
	div.append("<label for='itemPrice'>Родительская категория</label>");
	
	var parent_id =0;
	if(action==1) { 
		parent_id = cat.parent_id;
	} 
	
	$.get( "/categories/list", function(json) {
		if(json.result==true){
			var categories = json.data;
			var select = $("<select class='form-control'></select>");
			select.attr("id","catParent");
			for(var i=0;i<categories.length;i++){
				
				if(action==1) {		
					if(categories[i].id==cat.id) {
						continue;
					}
				}
				
				if(categories[i].id==parent_id) {
					select.append("<option selected value="+categories[i].id+">"+categories[i].name+"</option>");
				}else {
					select.append("<option value="+categories[i].id+">"+categories[i].name+"</option>");
				}
			}
			div.append(select);
		}
	});
		
	div.appendTo(form);
	
	$('#modalBlock').append(form);
	
	var btn = $('<button>Сохранить</button>');
	btn.attr("type","button");
	if(action==1) {
		btn.attr("onClick","saveRecord("+cat.id+")");
	} else {
		btn.attr("onClick","insertRecord()");
	}
	btn.addClass("btn btn-primary");
	btn.attr("data-dismiss","modal");
	btn.appendTo($("#modalButtons"));
	
	btn = $('<button>Отмена</button>');
	btn.attr("type","button");
	btn.attr("data-dismiss","modal");
	btn.addClass("btn btn-secondary");
	btn.appendTo($("#modalButtons"));
}

function deleteBtn(id,name){
	var btn = document.createElement("button");
	btn.type="button";
	btn.className="btn btn-default";
	btn.setAttribute("aria-label","Left Align");
	
	var span = document.createElement("span");
	span.className = "glyphicon glyphicon-trash";
	span.setAttribute("aria-hidden","true");
	btn.appendChild(span);

	btn.setAttribute("onClick", "deleteCat("+id+", '"+name+"')");
	return btn;
}

/* raise alert for user */
function raiseAlert(level, msg){
	removeAlertBlock();
	setTimeout(	removeAlertBlock, 5000);
	switch (level) {
		case 0:
			warningClass = "alert-success";
		break;
		
		case 1:
			warningClass = "alert-info";
		break;
		
		case 2:
			warningClass = "alert-warning";
		break;
		
		case 3:
			warningClass= "alert-danger";
		break;
	};
	
	var div = document.createElement("div");
	div.className = "alert "+warningClass+" alert-dismissible"; //class="hide fade"
	div.id = "alertMessage";
	
	var btn = document.createElement("button");
	btn.type="button";
	btn.className="close";
	btn.setAttribute("data-dismiss","alert");
	btn.innerHTML = "&times;";
	div.appendChild(btn);
	
	var span = document.createElement("strong");
	span.innerHTML = "Внимание!";
	div.appendChild(span);
	
	var text = document.createElement("span");
	text.innerHTML = " "+msg;
	div.appendChild(text);

	var contentBlock = document.getElementById("alertBlock"); 
	contentBlock.appendChild(div);
	
	$('#alertBlock').fadeIn(300);
}

/* remove alert block */
function removeAlertBlock(){
	var alertMessage = document.getElementById("alertMessage"); 
	var contentBlock = document.getElementById("alertBlock"); 
	if(alertMessage) {
		$('#alertBlock').fadeOut(600, function() {   
			contentBlock.removeChild(alertMessage);
		});
		return true;
	} else {
		return false;
	}
}


